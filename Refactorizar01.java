/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */



package refactorizar;

//la extension de lo que importas se escribe.
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
//Las clases empiezan con letra en Mayuscula
public class Refactorizar01 {
	//Nombres de constantes: CONSTANT_CASE. Todo el may�sculas, separando con barra baja.
	final static String BIENVENIDO = "Bienvenido al programa";
	final static int DIASTOTALES = 7; 
	public static void main(String[] args) {
		//Una sola declaracion por linea, nombres de las variables mas claros.
		
		Scanner lector = new Scanner(System.in);
		
		System.out.println(BIENVENIDO);
		
		System.out.println("Introduce tu dni");
		String dni = lector.nextLine();
		
		System.out.println("Introduce tu nombre");
		String nombre = lector.nextLine();
		
		
		//Las variables locales se deben inicializar en el momento de declararlas o justo despu�s. 
		//Se declaran justo antes de su uso, para reducir su �mbito.
		int numero1 = 7;
		int numero2 = 16;
		int numeroResultado = 25;
		
		if(((numero1>numero2)
				||(numeroResultado % 5 != 0))
				&&(((numeroResultado * 3) -1) > (numero2/numeroResultado)))
		{
			System.out.println("Se cumple la condici�n");
		}
		
		numeroResultado = numero1 + (numero2 * numeroResultado) + (numero2/numero1);
		
		//Los arrays se pueden inicializar en bloque
		//Los arrays tienen los corchetes [ ] unidos a su tipo de datos
		String [] diasDeLaSemana = {"Lunes", "Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
		
		recorrerArray(diasDeLaSemana);
		
		lector.close();
	}
	
	//Nombre de los m�todos: lowerCamelCase. Suelen ser verbos o frases.
	 private static void recorrerArray(String [] diaDeLaSemana)
	{ 
		//No poner numeros sino variables con informacion.
		for(int dia = 0; dia < DIASTOTALES; dia++) 
		{
			System.out.println("El dia de la semana en el que te encuentras [" + (dia+1) + "-7] es el dia: " + diaDeLaSemana[dia]);
		}
	}
	
}
