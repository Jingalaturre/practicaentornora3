
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

//Nombre paquete en minusculas
package refactorizar;


import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
//Nombre clase empieza en mayuscula
public class Refactorizar02 {

	//Nombres de constantes: CONSTANT_CASE. Todo el may�sculas, separando con barra baja.
	final static Scanner LECTOR =  new Scanner(System.in);
	public static void main(String[] args) 
	{
	
		//Una sola declaracion, variables se escriben holaMundo, declaro directamente si la voy a utilizar justo despues.
		
		int totalNotas = 10;
		
		//utilizar el nombre de la variable en vez de un numero que no representa nada en el for.
		int [] notas = new int[totalNotas];
		//dar tipo variable dentro del for.
		for(int notaAlumno = 0; notaAlumno < totalNotas; notaAlumno++)
		{
			System.out.println("Introduce nota de alumno");
			notas[notaAlumno] = LECTOR.nextInt();
		}	
		
		System.out.println("La nota media del alumno es: " + notaMedia(notas));
		
		LECTOR.close();
	}
	
	//Nombre de los m�todos: lowerCamelCase. Suelen ser verbos o frases.
	//Escribir nombres claros, para que se entienda de que va el programa.
	private static double notaMedia(int[] valorNota)
	{
		double sumaNotas = 0;
		
		for(int notas=0;notas<10;notas++) 
		{
			sumaNotas=sumaNotas+valorNota[notas];
		}
		return sumaNotas/10;
	}
	
}
