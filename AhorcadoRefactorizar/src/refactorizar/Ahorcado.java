package refactorizar;

import java.io.File;
import java.util.Scanner;

public class Ahorcado {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];

	public static void main(String[] args) {
		
		String palabraSecreta;
		
		File fich = ficheroAhorcado();
		Scanner inputFichero = null;
		ficheroPalabras(fich, inputFichero);
		
		Scanner input = new Scanner(System.in);
		palabraSecreta = palabraParaAdivinar();
		char[][] caracteresPalabra = matrizPalabraSecreta(palabraSecreta);
		String caracteresElegidos = "";
		int fallos;
		
		boolean acertado;
		System.out.println("Acierta la palabra");
		do {
			separador();
			imprimePalabra(caracteresPalabra);
			caracteresElegidos = eligeLetra(input, caracteresElegidos);			
			fallos = 0;
			boolean encontrado;
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = letraEncontrada(caracteresPalabra, caracteresElegidos, j);
				fallos = falloEncontrado(fallos, encontrado);
			}
			mostrarAhorcado(fallos);
			gameOverPerder(palabraSecreta, fallos);
			acertado = gameOverAcertado(caracteresPalabra);
		} while (!acertado && fallos < FALLOS);
			input.close();
	}

	
	private static boolean letraEncontrada(char[][] caracteresPalabra, String caracteresElegidos, int j) {
		boolean encontrado;
		encontrado = false;
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
				caracteresPalabra[1][i] = '1';
				encontrado = true;
			}
		}
		return encontrado;
	}

	private static File ficheroAhorcado() {
		String palabraSecreta, ruta = "src\\Refactorizar\\ahorcado.txt";
		File fich = new File(ruta);
		return fich;
	}

	private static char[][] matrizPalabraSecreta(String palabraSecreta) {
		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		return caracteresPalabra;
	}

	private static void separador() {
		System.out.println("####################################");
	}

	private static boolean gameOverAcertado(char[][] caracteresPalabra) {
		boolean acertado;
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}

	private static void gameOverPerder(String palabraSecreta, int fallos) {
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
	}

	private static int falloEncontrado(int fallos, boolean encontrado) {
		if (!encontrado)
			fallos++;
		return fallos;
	}

	private static String palabraParaAdivinar() {
		String palabraSecreta;
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		return palabraSecreta;
	}

	private static String eligeLetra(Scanner input, String caracteresElegidos) {
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static void imprimePalabra(char[][] caracteresPalabra) {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

	private static void ficheroPalabras(File fich, Scanner inputFichero) {
		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

	private static void mostrarAhorcado(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
		
	}

}
