package refactorizar;

import java.util.Scanner;

public class GeneradorReal {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		menu();
		int longitud = pedirLongitud(scanner);
		int opcion = opcionPassword(scanner);
		String password = "";
		switch (opcion) {
		case 1:
			password = contraseñaCaracteres(longitud, password);
			break;
		case 2:
			password = contraseñaNumeros(longitud, password);
			break;
		case 3:
			password = contraseñaLetrasYCaract(longitud, password);
			break;
		case 4:
			password = contraseñaLetrasNumChars(longitud, password);
			break;
		}

		System.out.println(password);
		scanner.close();
	}

	
	
	private static String contraseñaLetrasNumChars(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int num;
			num = (int) (Math.random() * 3);
			if (num == 1) {
				password = contraseñaCaracteres2(password);
			} else if (num == 2) {
				password = contraseñaCaracteresEsp(password);
			} else {
				int numero4 = generarNumero();
				password += numero4;
			}
		}
		return password;
	}

	private static String contraseñaLetrasYCaract(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int num;
			num = (int) (Math.random() * 2);
			if (num == 1) {
				password = contraseñaCaracteres2(password);
			} else {
				password = contraseñaCaracteresEsp(password);
			}
		}
		return password;
	}

	private static String contraseñaCaracteresEsp(String password) {
		char caracter3 = caracteresEspeciales();
		password += caracter3;
		return password;
	}

	private static char caracteresEspeciales() {
		char caracter3;
		caracter3 = (char) ((Math.random() * 15) + 33);
		return caracter3;
	}

	private static String contraseñaCaracteres(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			password = contraseñaCaracteres2(password);
		}
		return password;
	}

	private static String contraseñaNumeros(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int numero2 = generarNumero();
			password += numero2;
		}
		return password;
	}

	private static int generarNumero() {
		int numero2;
		numero2 = (int) (Math.random() * 10);
		return numero2;
	}

	private static String contraseñaCaracteres2(String password) {
		char letra1 = generarCaracter();
		password += letra1;
		return password;
	}

	private static char generarCaracter() {
		char letra1;
		letra1 = (char) ((Math.random() * 26) + 65);
		return letra1;
	}

	private static int opcionPassword(Scanner scanner) {
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		return opcion;
	}

	private static int pedirLongitud(Scanner scanner) {
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		return longitud;
	}

	private static void menu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		
	}

}